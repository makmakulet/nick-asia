<?php
/**
 * Homepage
 *
 */
?>
<?php require_once("views/head.php") ?>
<?php require_once("views/back-to-nick.php") ?>
<?php require_once("views/page-header.php") ?>
<?php require_once("views/intro.php") ?>
<?php require_once("views/game.php") ?>
<?php require_once("views/form.php") ?>
<?php require_once("views/footer.php") ?>