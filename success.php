<?php
/**
 * Success
 *
 */
?>

<?php require_once("views/head.php") ?>
<?php require_once("views/back-to-nick.php") ?>

<div class="success-page">
  <section class="wrap game-banner">
    <img src="<?php echo $assets_url; ?>/images/nickfiesta.png" class="game-image" alt="<?php echo $site_title; ?>" />
  </section>

  <section class="wrap message-wrap">
    <div class="success-message">
        <p>Submission Successful.</p>
        <p class="good-luck">GoodLuck!</p>
      </div>
  </section>
</div>

<?php require_once("views/footer.php") ?>