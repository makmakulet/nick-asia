<?php
require_once("dotenv.php");


$DB_NAME = getenv('DB_NAME');
$DB_HOST = getenv('DB_HOST');
$DB_USER = getenv('DB_USER');
$DB_PASS = getenv('DB_PASS');


/* Connect to a MySQL database using driver invocation */
$dsn = "mysql:dbname={$DB_NAME};host={$DB_HOST}";
$user = $DB_USER;
$password = $DB_PASS;

try {
  $pdo = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo $e;
}
