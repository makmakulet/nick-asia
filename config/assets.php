<?php
$assets_url = "assets";

$site_title = 'Nickelodeon Fiesta 2019';
$site_description = 'Launch yourself into a fabulous carnival of adventures! Get a chance to win a trip to Nickelodeon Fiesta 2019!';
$site_keywords = 'Nickelodeon, Nickelodeon Fiesta';
?>