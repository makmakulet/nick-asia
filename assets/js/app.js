/**
 * Game
 *
 */

// Polyfill, just in case
if (!Object.values) {
  Object.values = function(o) {
    return Object.keys(o).map(function(k) {
      return o[k];
    });
  };
}

//----------------------------------------------------------------------------

$(function() {
  var $gamerForm = $(".gamer-form");
  var baseUrl = "";

  // Messages
  var errors = {
    DEFAULT_ERROR: "Something went wrong. Please try again.",
    PHONE_MUST_BE_NUMBER: "Phone Number field must be numbers!",
    WRONG_CHARACTERS: "Wrong characters! Please find the correct characters.",
    FIELDS_REQUIRED: "All fields are required",
    TERMS_NOT_ACCEPTED:
      "Please accept Terms and Conditions (item 1, 2 & 3) to submit the form!"
  };

  //----------------------------------------------------------------------------

  // Init dropped
  var dropped = {};
  for (var i = 0; i < 6; i++) {
    dropped[i] = "";
  }

  // Correct characters
  var correctCharacters = [0, 2, 3, 4, 5, 6];

  //----------------------------------------------------------------------------

  // Draggable Characters
  var $srcFrames = $(".source-frames");

  $(".source-frames .character").draggable({
    revert: true,

    drag: function() {
      $(this).addClass("active");
      $srcFrames.addClass("active");
    },

    stop: function() {
      $(this).removeClass("active");
      $srcFrames.removeClass("active");
    }
  });

  //----------------------------------------------------------------------------

  // Droppable Baskets
  $(".destination-frames .basket").each(function(key) {
    var basket = $(this);

    basket.droppable({
      hoverClass: "highlight",

      drop: function(event, ui) {
        var item = ui.draggable;

        if (!basket.hasClass("filled")) {
          addToBasket(basket, item, key);
        }
      }
    });

    // Remove button
    basket.on("click", "button", function() {
      removeFromBasket(basket, key);
    });
  });

  //----------------------------------------------------------------------------

  // Drop character to basket
  function addToBasket(basket, item, basketKey) {
    var itemContent = $(item)
      .find("span")
      .html();

    var key = item.data("key");

    $(basket)
      .addClass("filled")
      .find("span")
      .html(itemContent);

    dropped[basketKey] = key;
  }

  //----------------------------------------------------------------------------

  // Remove character from basket
  function removeFromBasket(basket, basketKey) {
    $(basket)
      .removeClass("filled")
      .find("span")
      .empty();

    dropped[basketKey] = "";
  }

  //----------------------------------------------------------------------------

  // Datepicker
  $("#dob").datepicker({
    dateFormat: "dd/mm/yy",
    changeMonth: true,
    changeYear: true,
    yearRange: "-100y:c+nn",
    maxDate: "-1d"
  });

  //----------------------------------------------------------------------------

  // Loaders
  function addLoader() {
    $gamerForm.addClass("loading");
  }

  function removeLoader() {
    $gamerForm.removeClass("loading");
  }

  //----------------------------------------------------------------------------

  // Validation, mostly based on num-noms
  function diffCharacters() {
    var droppedValues = Object.values(dropped).sort();
    return JSON.stringify(droppedValues) != JSON.stringify(correctCharacters);
  }

  function hasBlanks() {
    // Check required fields
    var blank = true;

    $(".required").each(function() {
      if (!$.trim($(this).val())) {
        blank = true;
        return false;
      }

      blank = false;
    });

    return blank;
  }

  //----------------------------------------------------------------------------

  // On submit
  $gamerForm.on("submit", function(e) {
    // Check selected characters
    if (diffCharacters()) {
      alert(errors.WRONG_CHARACTERS);
      return false;
    }

    // Add loader
    addLoader();

    // Check required
    if (hasBlanks()) {
      alert(errors.FIELDS_REQUIRED);
      removeLoader();
      return false;
    }

    // Check phone number
    if (isNaN($(".phone").val())) {
      alert(errors.PHONE_MUST_BE_NUMBER);
      removeLoader();
      return false;
    }

    // Required clauses
    if (
      $("#clause1").is(":checked") == false ||
      $("#clause2").is(":checked") == false ||
      $("#clause3").is(":checked") == false
    ) {
      alert(errors.TERMS_NOT_ACCEPTED);
      removeLoader();
      return false;
    }

    // Passed all validations
    var fd = new FormData();
    fd.append("answer", $("textarea[name='answer']").val());
    fd.append("child_name", $("input[name='child_name']").val());
    fd.append("parent_name", $("input[name='parent_name']").val());
    fd.append("child_email", $("input[name='child_email']").val());
    fd.append("parent_email", $("input[name='parent_email']").val());
    fd.append("birthday", $("input[name='dob']").val());
    fd.append("address", $("input[name='address']").val());
    fd.append("phone", $("input[name='phone']").val());
    fd.append("clause1", $("#clause1").is(":checked") ? 1 : 0);
    fd.append("clause2", $("#clause2").is(":checked") ? 1 : 0);
    fd.append("clause3", $("#clause3").is(":checked") ? 1 : 0);
    fd.append("clause4", $("#clause4").is(":checked") ? 1 : 0);

    // XHR call
    var xhr = $.ajax({
      type: "POST",
      url: baseUrl + "/process.php",
      contentType: false,
      cache: false,
      processData: false,
      data: fd
    });

    // Success
    xhr.done(function(res) {
      var data = JSON.parse(res);
      var message = data && data.message;

      if (message == "Success") {
        window.location = baseUrl + "/success.php";
      } else {
        alert(message || errors.DEFAULT_ERROR);
      }
    });

    // Error
    xhr.fail(function() {
      alert(errors.DEFAULT_ERROR);
    });

    // Remove loader on XHR complete
    xhr.always(function() {
      removeLoader();
    });

    return false;
  });

  //----------------------------------------------------------------------------

  // Scroll to bottom
  // if ($(".success-page").length) {
  //   $("html, body").animate({ scrollTop: $(document).height() }, 500);
  // }
});
