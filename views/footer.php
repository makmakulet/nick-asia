<?php
/**
 * Foot
 *
 */
include("config/assets.php");
?>


<footer class="page-footer">
  <div class="shops">
    <img src="<?php echo $assets_url; ?>/images/bg/shophouse.png" />
  </div>
  <div class="sponsors">
    <div class="wrap">
      <div class="item">
        <h3>Presented by</h3>
        <img class="clarke-quay" src="<?php echo $assets_url; ?>/images/sponsors/clarke-quay.png" alt="Clarke Quay" />
      </div>

      <div class="item">
        <h3>Singapore Festival of Fun organized by</h3>
        <img class="magic-rock" src="<?php echo $assets_url; ?>/images/sponsors/magic-rock.png" alt="Magic Rock" />
      </div>

      <div class="item">
        <h3>Supported by</h3>
        <img class="sg-tourism-board" src="<?php echo $assets_url; ?>/images/sponsors/sg-tourism-board.png" alt="Singapore Tourism Board" />
      </div>

      <div class="item">
        <h3>Held in</h3>
        <img class="sg"src="<?php echo $assets_url; ?>/images/sponsors/sg.png" alt="Singapore. Passion Made Possible" />
      </div>
    </div>
  </div>
</footer>

</body>
</html>
