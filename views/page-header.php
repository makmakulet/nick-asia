<?php
/**
 * Page Header
 *
 */
include("config/assets.php");
?>

<div class="page-header">
  <div class="wrap">
    <div class="contents">
      <img src="<?php echo $assets_url; ?>/images/nickfiesta.png" class="site-logo" alt="<?php echo $site_title; ?>" />
      <div class="details">
        <p class="dates">23 & 24 March</p>
        <p class="location">10AM - 6PM @ Clarke Quay</p>
      </div>
      <img src="<?php echo $assets_url; ?>/images/characters.png" class="featured-characters" />
    </div>
  </div>
</div>