<?php
/**
 * Intro
 *
 */

include("config/assets.php");
?>
<section class="contest-intro">
  <div class="wrap">
    <h2 class="section-title">NEW GAMES, NEW THRILLS!</h2>
    <p>Launch yourself into a fabulous <strong>carnival of adventures!</strong> Play at the game booths featuring themes from the biggest Nickelodeon shows, where you can get a feel of nostalgic trades like street cobblers, kacang puteh sellers or traditional bakers, and also stand to redeem limited edition <strong>Nickelodeon goodie bags*!</strong></p>

    <div class="activities">
      <div class="activity">
        <h2 class="section-title">Set Sail with nickelodeon!</h2>
        <p>Journey on the first ever <strong>Nickelodeon Fiesta River Cruise**</strong> and get to know Singapore like never before. An experience you definitely don't want to miss!</p>
      </div>

      <div class="activity">
        <h2 class="section-title">Let's Get Crafty!</h2>
        <p>Create your own <strong>Nickelodeon masterpieces***</strong> - paint your own lantern, create flower garlands, and more! Unleash your imagination and get to bring home your creations!</p>
      </div>
    </div>

    <ul>
      <li>*While stocks last. Terms and conditions apply.</li>
      <li>**Limited scheduled times – free registration at Nickelodeon Fiesta River Cruise booth required.</li>
      <li>***Limited scheduled times – free registration at Art & Craft Workshop booth required.</li>
    </ul>
  </div>
</section>