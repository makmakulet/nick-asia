<?php
/**
 * Game
 *
 */
include("config/assets.php");

$game_title = "Which Characters Can You Find?";
$character_count = 8;
$basket_count = 6;
?>

<div class="game-section">
  <div class="wrap">
    <div>
      <section class="game-title">
        <h2 class="section-title">Spot the characters!</h2>
        <img src="<?php echo $assets_url; ?>/images/win.png" class="win-badge" />
      </section>

      <img class="game-image" src="<?php echo $assets_url; ?>/images/game.jpg" alt="<?php echo $game_title; ?>" />

      <section class="instructions">
        <h2 class="section-title">Which characters can you find?</h2>
        <p>Drag and drop the 6 hidden characters from the image above to win a trip to Nickelodeon Fiesta 2019!</p>

        <div class="source-frames">
          <?php for($i=0; $i<$character_count; $i++): ?>
            <div class="frame frame-<?php echo $i + 1; ?>">
              <div data-key="<?php echo $i; ?>" class="character">
                <span>
                  <img src="<?php echo $assets_url; ?>/images/characters/char<?php echo $i + 1; ?>.png" />
                </span>
              </div>
            </div>
          <?php endfor; ?>
        </div>
      </section>

      <section class="frame-answers">
        <div class="destination-frames">
          <?php for($i=0; $i<$basket_count; $i++): ?>
            <div class="frame">
              <div class="basket">
                <span></span>
                <button class="remove-button"><em></em> Remove</button>
              </div>
            </div>
          <?php endfor; ?>
        </div>
      </section>
    </div>
  </div>
</div>
