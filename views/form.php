<?php
/**
 * Form + Terms
 *
 */
?>

<section class="form-section">
  <div class="wrap">
    <form class="gamer-form" action="success.php" method="post" enctype="multipart/form-data">

      <div class="top">
        <h2 class="section-title">Why would you like to come to Singapore for Nickelodeon Fiesta?</h2>
        <p>Most creative answers wins!</p>

        <div class="row">
          <div class="col single">
            <textarea name="answer" class="required" placeholder="Tell us why here"></textarea>
          </div>
        </div>
      </div>

      <div class="wrap">
        <div class="row">
          <div class="col">
            <label>Name of child</label>
            <input name="child_name" type="text" class="required" />
          </div>
          <div class="col">
            <label>Name of parent / guardian</label>
            <input name="parent_name" type="text" class="required" />
          </div>
        </div>

        <div class="row">
          <div class="col">
            <label>Child's Email Address</label>
            <input name="child_email" type="text" class="required" />
          </div>
          <div class="col">
            <label>Parent / Guardian's Email Address</label>
            <input name="parent_email" type="text" class="required" />
          </div>
        </div>

        <div class="row">
          <div class="col">
            <label>Date of birth</label>
            <input name="dob" id="dob" type="text" class="required" autocomplete="false" />
          </div>
          <div class="col">
            <label>Address</label>
            <input name="address" type="text" class="required" />
          </div>
        </div>

        <div class="row">
          <div class="col left">
            <label>Contact Number</label>
            <input name="phone" type="text" class="required phone" />
          </div>
        </div>
      </div>

      <div class="form-footer">
        <div class="terms-section wrap">
          <h3 class="terms-title">Contest Details</h3>
          <ul>
            <li>Prize: Flights and 2 Nights Accommodation for 2 Adults & 2 Children.</li>
            <li>1 family will be selected.</li>
            <li>Contest ends 2 March 2019.</li>
            <li>Terms and conditions <a href="http://web2.mtvasia.com/microsite/pawsperous/assets/terms_and_conditions.pdf" target="_blank">apply</a>.</li>
          </ul>

          <div class="clauses">
            <p class="pseudo-checkbox">
              <input name="clause1" id="clause1" type="checkbox" />
              <label for="clause1">I have read and agree to the terms and conditions.</label>
            </p>

            <p class="pseudo-checkbox">
              <input name="clause2" id="clause2" type="checkbox" />
              <label for="clause2">I confirm that I am above 18 years of age as of today; I am the parent/guardian and I have consented to my child entering this Contest (if applicable)</label>
            </p>

            <p class="pseudo-checkbox">
              <input name="clause3" id="clause3" type="checkbox" />
              <label for="clause3">I hereby consent to be contacted at the phone number and/or email address submitted for this Contest for the purpose of further notification and/or identification.</label>
            </p>

            <p class="pseudo-checkbox">
              <input name="clause4" id="clause4" type="checkbox" />
              <label for="clause4">[OPTIONAL] Additionally, I consent to allow for my personal information to be stored by Nickelodeon for future notification of all Nickelodeon related campaigns / events.</label>
            </p>
          </div>

          <p>© 2019 Viacom International Inc. All Rights Reserved.</p>

          <div class="submit-button-container row">
            <button class="form-submit-button" type="submit">Submit</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>