<?php
/**
 * Head
 *
 */

include("config/assets.php");
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133668716-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-133668716-1');
  </script>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title><?php echo $site_title; ?></title>
  <meta name="description" content="<?php echo $site_description; ?>" />
  <meta name="keywords" content="<?php echo $site_keywords ?>" />

  <link rel="shortcut icon" href="<?php echo $assets_url; ?>/images/favicon.png" />
  <link href="<?php echo $assets_url; ?>/styles/main.min.css" rel="stylesheet">
  <script async src="<?php echo $assets_url; ?>/js/app.min.js"></script>
</head>
<body>