<?php
/**
 * Back to Nick link
 *
 */

include("config/assets.php");
?>

<div class="back-to-nick">
  <div class="wrap">
    <a href="http://www.nick-asia.com">
      <span>Back to</span><img src="<?php echo $assets_url; ?>/images/nick-logo.png" alt="Nickelodeon" />
    </a>
  </div>
</div>
