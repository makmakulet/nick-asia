<?php
require_once('config/db.php');

// Responses
$success = array('message' => 'Success');
$error = array('message' => 'Something went wrong. Please try again.');

$sql = 'INSERT INTO `nickfiesta2019` ' .
  '(child_name, parent_name, answer, birthday, child_email, parent_email, phone, address, clause1, clause2, clause3, clause4 ) ' .
'VALUES ' .
  '(:child_name, :parent_name, :answer, :birthday, :child_email, :parent_email, :phone, :address, :clause1, :clause2, :clause3, :clause4 )';

$query = $pdo->prepare($sql);

$query->bindValue(':child_name', $_REQUEST['child_name'], PDO::PARAM_STR);
$query->bindValue(':parent_name', $_REQUEST['parent_name'], PDO::PARAM_STR);
$query->bindValue(':answer', $_REQUEST['answer'], PDO::PARAM_STR);
$query->bindValue(':birthday', date('Y-m-d', strtotime($_REQUEST['birthday'])), PDO::PARAM_STR);
$query->bindValue(':child_email', $_REQUEST['child_email'], PDO::PARAM_STR);
$query->bindValue(':parent_email', $_REQUEST['parent_email'], PDO::PARAM_STR);
$query->bindValue(':phone', $_REQUEST['phone'], PDO::PARAM_STR);
$query->bindValue(':address', $_REQUEST['address'], PDO::PARAM_STR);
$query->bindValue(':clause1', $_REQUEST['clause1'], PDO::PARAM_BOOL);
$query->bindValue(':clause2', $_REQUEST['clause2'], PDO::PARAM_BOOL);
$query->bindValue(':clause3', $_REQUEST['clause3'], PDO::PARAM_BOOL);
$query->bindValue(':clause4', $_REQUEST['clause4'], PDO::PARAM_BOOL);

$insert = $query->execute();

// If properly inserted, return with success
if ($insert) {
  echo json_encode($success);
  return;
}

// Return error
echo json_encode($error);
